(* Once you are done writing the code, remove this directive,
   whose purpose is to disable several warnings. *)
[@@@warning "-27-34-37-39"]

open Fold
open Mu

module CStack = struct
  type 'a f = NYI

  let map _ _ = failwith "NYI"
end

module Stack = Mu (CStack)

let empty () = failwith "NYI"

let push n ns = failwith "NYI"

let rec from_stack _ = failwith "NYI"
