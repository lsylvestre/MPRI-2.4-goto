(* Once you are done writing the code, remove this directive,
   whose purpose is to disable several warnings. *)
[@@@warning "-27-33-34-37-39"]

open Fold
open Nat

(*****************************************************************)
(* Algebra                                                       *)
(*****************************************************************)

module AlgFac = struct
  module F = CNat

  type carrier1 = NYI

  type carrier2 = NYI

  let alg1 _fac _id = failwith "NYI"

  let alg2 _fac _id = failwith "NYI"
end

(*****************************************************************)
(* Fixpoint                                                      *)
(*****************************************************************)

let fac _ = failwith "NYI"

(*****************************************************************)
(* Tests                                                         *)
(*****************************************************************)

let%test _ =
  let x = Spec.Zero in
  Spec.fac x = fac (from_nat x)

let%test _ =
  let x = Spec.Succ Spec.Zero in
  Spec.fac x = fac (from_nat x)

let%test _ =
  let x = Spec.Succ (Spec.Succ Spec.Zero) in
  Spec.fac x = fac (from_nat x)

let%test _ =
  let x = Spec.Succ (Spec.Succ (Spec.Succ Spec.Zero)) in
  Spec.fac x = fac (from_nat x)
