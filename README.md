# lecture-MPRI-2.4-nbe

## Setup

This project needs `dune`, `ppx_inline_test` and `ppx_deriving` to
build. You can get them through

    opam install dune.2.7 ppx_inline_test ppx_deriving

KNOWN ISSUE: the integration of `ppx_inline_test` and merlin is known
to cause problem if you're using `ocaml-migrate-parsetree > 1.8.0`. Do

    opam install ocaml-migrate-parsetree.1.8.0

to resolve this issue (and restart merlin).

## Development

Once your programming environment is setup, you can build the project with

    dune build

You can also build and run the test suite with

    dune runtest

If you want to run individual tests, use

    ./runtest.sh exercises/total.ml

to only run the inline tests in the file `exercises/total.ml`, or

    ./runtest.sh exercises/total.ml:60

to only run the inline test defined at line 60 in the file
`exercises/total.ml`.


## Order of business

All the datastructures (stack, tree, nat, rose) and programs to
implement are specified in the module `exercises/spec.ml`.

Your host suggests that you complete the exercises in the following
order:
  1. `exercices/magic.ml`
  1. `lib/Catamorphism.ml`
  1. `exercises/stack.ml`
  1. `exercises/total.ml`
  1. Need more? `exercises/tree.ml`
  1. Need more? `exercises/depth.ml`
  1. `lib/Curry.ml`
  1. `exercises/cat.ml`
  1. Need more? `exercises/shunt.ml`
  1. Need more? `exercises/depths.ml`
  1. Need more? `lib/Curryfix.ml`
  1. Need more? `exercises/zip_plus.ml`
  1. `lib/Paramorphism.ml`
  1. `exercises/nat.ml`
  1. `exercises/fibonacci.ml`
  1. Need more? `exercises/factorial.ml`
  1. `lib/Mutual.ml`
  1. `exercises/rose.ml`
  1. `exercises/flatten.ml`

Curious for more? Check out [Unifying structured recursion schemes](https://doi.org/10.1017/S0956796815000258) and [Adjoint folds and unfolds](https://doi.org/10.1016/j.scico.2012.07.011).
