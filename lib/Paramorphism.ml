(* Once you are done writing the code, remove this directive,
   whose purpose is to disable several warnings. *)
[@@@warning "-33-39"]

open Algebra
open Mu

(*****************************************************************)
(* Generic recursor                                              *)
(*****************************************************************)

module Fix (A : AlgPara) = struct
  module MuF = Mu (A.F)
  open MuF

  let rec fix1 _ = failwith "NYI"

  and fix2 _ = failwith "NYI"
end

(* Section 5.5 *)
